package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TablesControllerTest {

    @Autowired
    private MockMvc mock;

    @Test
    void should_get_full_plus_table() throws Exception {
        mock.perform(get("/api/tables/plus"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
                .andExpect(content().string(
                        "1+1=2\r\n" +
                                "2+1=3\t2+2=4\r\n" +
                                "3+1=4\t3+2=5\t3+3=6\r\n" +
                                "4+1=5\t4+2=6\t4+3=7\t4+4=8\r\n" +
                                "5+1=6\t5+2=7\t5+3=8\t5+4=9\t5+5=10\r\n" +
                                "6+1=7\t6+2=8\t6+3=9\t6+4=10\t6+5=11\t6+6=12\r\n" +
                                "7+1=8\t7+2=9\t7+3=10\t7+4=11\t7+5=12\t7+6=13\t7+7=14\r\n" +
                                "8+1=9\t8+2=10\t8+3=11\t8+4=12\t8+5=13\t8+6=14\t8+7=15\t8+8=16\r\n" +
                                "9+1=10\t9+2=11\t9+3=12\t9+4=13\t9+5=14\t9+6=15\t9+7=16\t9+8=17\t9+9=18\r\n"));
    }

    @Test
    void should_get_full_multiply_table() throws Exception {
        mock.perform(get("/api/tables/multiply"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
                .andExpect(content().string(
                        "1*1=1\r\n" +
                                "2*1=2\t2*2=4\r\n" +
                                "3*1=3\t3*2=6\t3*3=9\r\n" +
                                "4*1=4\t4*2=8\t4*3=12\t4*4=16\r\n" +
                                "5*1=5\t5*2=10\t5*3=15\t5*4=20\t5*5=25\r\n" +
                                "6*1=6\t6*2=12\t6*3=18\t6*4=24\t6*5=30\t6*6=36\r\n" +
                                "7*1=7\t7*2=14\t7*3=21\t7*4=28\t7*5=35\t7*6=42\t7*7=49\r\n" +
                                "8*1=8\t8*2=16\t8*3=24\t8*4=32\t8*5=40\t8*6=48\t8*7=56\t8*8=64\r\n" +
                                "9*1=9\t9*2=18\t9*3=27\t9*4=36\t9*5=45\t9*6=54\t9*7=63\t9*8=72\t9*9=81\r\n"));
    }

    @Test
    void should_get_plus_table_by_start() throws Exception {
        mock.perform(
                get("/api/tables/plus")
                        .param("start", "7")
        )
                .andExpect(content().string(
                        "7+7=14\r\n" +
                                "8+7=15\t8+8=16\r\n" +
                                "9+7=16\t9+8=17\t9+9=18\r\n"
                ));
    }

    @Test
    void should_get_multiply_table_by_start() throws Exception {
        mock.perform(
                get("/api/tables/multiply")
                        .param("start", "7")
        )
                .andExpect(content().string(
                        "7*7=49\r\n" +
                                "8*7=56\t8*8=64\r\n" +
                                "9*7=63\t9*8=72\t9*9=81\r\n"
                ));
    }

    @Test
    void should_not_get_plus_table_when_illegal_range() throws Exception {
        mock.perform(
                get("/api/tables/plus")
                        .param("start", "10")
                        .param("end", "12")
        )
                .andExpect(status().is(400));
    }

    @Test
    void should_not_get_multiply_table_when_illegal_range() throws Exception {
        mock.perform(
                get("/api/tables/multiply")
                        .param("start", "10")
                        .param("end", "12")
        )
                .andExpect(status().is(400));
    }

    @Test
    void should_get_expression_check_with_equal() throws Exception {
        mock.perform(
                post("/api/check")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{\n" +
                                "  \"operandLeft\": 4,\n" +
                                "  \"operandRight\": 5,\n" +
                                "  \"operation\": \"+\",\n" +
                                "  \"expectedResult\": 9,\n" +
                                "  \"checkType\": \"=\"\n" +
                                "}\n")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.correct").value(true));
    }

    @Test
    void should_get_expression_check_with_less() throws Exception {
        mock.perform(
                post("/api/check")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{\n" +
                                "  \"operandLeft\": 4,\n" +
                                "  \"operandRight\": 5,\n" +
                                "  \"operation\": \"*\",\n" +
                                "  \"expectedResult\": 30,\n" +
                                "  \"checkType\": \"<\"\n" +
                                "}\n")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.correct").value(true));
    }

    @Test
    void should_get_expression_check_with_greater() throws Exception {
        mock.perform(
                post("/api/check")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{\n" +
                                "  \"operandLeft\": 5,\n" +
                                "  \"operandRight\": 5,\n" +
                                "  \"operation\": \"*\",\n" +
                                "  \"expectedResult\": 24,\n" +
                                "  \"checkType\": \">\"\n" +
                                "}\n")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.correct").value(true));
    }

    @Test
    void should_get_exception_that_check_request() throws Exception {
        mock.perform(
                post("/api/check")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{\n" +
                                "  \"operandLeft\": 5,\n" +
                                "  \"operandRight\": 5,\n" +
                                "  \"operation\": \"wow\",\n" +
                                "  \"expectedResult\": 24,\n" +
                                "  \"checkType\": \">\"\n" +
                                "}\n")
        )
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_get_plus_table_use_json() throws Exception {
        mock.perform(
                get("/api/tables/plus/json")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .param("start", "4")
                        .param("end", "6")
        )
                .andExpect(jsonPath("$[0][0]").value("4+4=8"))
                .andExpect(jsonPath("$[2][0]").value("6+4=10"))
                .andExpect(jsonPath("$[2][2]").value("6+6=12"));
    }

    @Test
    void should_get_multiply_table_use_json() throws Exception {
        mock.perform(
                get("/api/tables/multiply/json")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .param("start", "4")
                        .param("end", "6")
        )
                .andExpect(jsonPath("$[0][0]").value("4*4=16"))
                .andExpect(jsonPath("$[2][0]").value("6*4=24"))
                .andExpect(jsonPath("$[2][2]").value("6*6=36"));
    }

    @Test
    void should_get_plus_table_use_html() throws Exception {
        mock.perform(
                get("/api/tables/plus/html")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .param("start", "4")
                        .param("end", "6")
        )
                .andExpect(status().isOk());
    }

    @Test
    void should_get_multiply_table_use_html() throws Exception {
        mock.perform(
                get("/api/tables/multiply/html")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .param("start", "4")
                        .param("end", "6")
        )
                .andExpect(status().isOk());
    }
}
