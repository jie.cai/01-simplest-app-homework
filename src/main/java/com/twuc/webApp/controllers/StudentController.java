package com.twuc.webApp.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {
    @PostMapping("/api/student")
    public String createStudent() {
        return "studentA";
    }
}
