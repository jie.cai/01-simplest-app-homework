package com.twuc.webApp.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.twuc.webApp.model.ArithmeticCheckRequest;
import com.twuc.webApp.model.ArithmeticCheckResult;
import com.twuc.webApp.model.ArithmeticExpression;
import com.twuc.webApp.model.ArithmeticTable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/api")
@Validated
public class TablesController {

    @ExceptionHandler(value = { ConstraintViolationException.class })
    public ResponseEntity<String> handleParamValidateException(Exception exception) {
        return ResponseEntity.status(400).body(exception.getMessage());
    }

    @GetMapping("/tables/plus")
    public String plus(
            @RequestParam(defaultValue = "1") @Max(8) @Min(1) Integer start,
            @RequestParam(defaultValue = "9") @Max(9) @Min(2) Integer end
    ) {
        return ArithmeticTable.plain(start, end, ArithmeticExpression::plus);
    }

    @GetMapping("/tables/multiply")
    public String multiply(
            @RequestParam(defaultValue = "1") @Max(8) @Min(1) Integer start,
            @RequestParam(defaultValue = "9") @Max(9) @Min(2) Integer end
    ) {
        return ArithmeticTable.plain(start, end, ArithmeticExpression::multiply);
    }

    @PostMapping("/check")
    public ArithmeticCheckResult check(
            @Valid @RequestBody ArithmeticCheckRequest checkRequest
    ) {
        return ArithmeticExpression.check(checkRequest);
    }

    @GetMapping("/tables/plus/json")
    public String plusJson(
            @RequestParam(defaultValue = "1") @Max(8) @Min(1) Integer start,
            @RequestParam(defaultValue = "9") @Max(9) @Min(2) Integer end
    ) throws JsonProcessingException {
        return ArithmeticTable.json(start, end, ArithmeticExpression::plus);
    }

    @GetMapping("/tables/multiply/json")
    public String multiplyJson(
            @RequestParam(defaultValue = "1") @Max(8) @Min(1) Integer start,
            @RequestParam(defaultValue = "9") @Max(9) @Min(2) Integer end
    ) throws JsonProcessingException {
        return ArithmeticTable.json(start, end, ArithmeticExpression::multiply);
    }

    @GetMapping("/tables/plus/html")
    public String plusHtml(
            @RequestParam(defaultValue = "1") @Max(8) @Min(1) Integer start,
            @RequestParam(defaultValue = "9") @Max(9) @Min(2) Integer end
    ) {
        return ArithmeticTable.html(start, end, ArithmeticExpression::plus);
    }

    @GetMapping("/tables/multiply/html")
    public String multiplyHtml(
            @RequestParam(defaultValue = "1") @Max(8) @Min(1) Integer start,
            @RequestParam(defaultValue = "9") @Max(9) @Min(2) Integer end
    ) {
        return ArithmeticTable.html(start, end, ArithmeticExpression::multiply);
    }
}
