package com.twuc.webApp;

import com.fasterxml.jackson.annotation.JsonValue;
import com.twuc.webApp.model.ArithmeticCheckResult;

import static com.twuc.webApp.model.ArithmeticCheckResult.make;

public enum ArithmeticCheckType {
    EQUAL("="),
    LESS("<"),
    GREATER(">");

    private final String name;

    public String getName() {
        return name;
    }

    ArithmeticCheckType(String name) {
        this.name = name;
    }

    public ArithmeticCheckResult check(Integer a, Integer b) {
        switch (this) {
            case EQUAL:
                return make(a.equals(b));
            case LESS:
                return make(a < b);
            case GREATER:
                return make(a > b);
            default:
                throw new AssertionError("Unknown checkType " + this);
        }
    }

    @JsonValue
    public String toString() {
        return this.name;
    }
}
