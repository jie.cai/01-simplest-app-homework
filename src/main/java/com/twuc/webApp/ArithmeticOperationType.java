package com.twuc.webApp;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ArithmeticOperationType {
    PLUS("+"),
    MULTIPLY("*");

    private final String name;

    public String getName() {
        return name;
    }

    ArithmeticOperationType(String name) {
        this.name = name;
    }

    public Integer calculate(Integer a, Integer b) {
        switch (this) {
            case PLUS:
                return a + b;
            case MULTIPLY:
                return a * b;
            default:
                throw new AssertionError("Unknown operations " + this);
        }
    }

    @JsonValue
    public String toString() {
        return this.name;
    }
}
