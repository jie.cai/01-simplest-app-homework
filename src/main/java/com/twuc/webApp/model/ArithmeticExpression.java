package com.twuc.webApp.model;

import com.twuc.webApp.ArithmeticOperationType;

public class ArithmeticExpression {

    public static ArithmeticExpression plus(Integer a, Integer b) {
        return new ArithmeticExpression(ArithmeticOperationType.PLUS, a, b);
    }
    public static ArithmeticExpression multiply(Integer a, Integer b) {
        return new ArithmeticExpression(ArithmeticOperationType.MULTIPLY, a, b);
    }
    public static ArithmeticCheckResult check(ArithmeticCheckRequest checkRequest) {
        return checkRequest
                .getCheckType()
                .check(
                        checkRequest
                                .getOperation()
                                .calculate(checkRequest.getOperandLeft(), checkRequest.getOperandRight()),
                        checkRequest.getExpectedResult()
                );
    }

    private String expression;
    private Integer result;

    String getExpression() {
        return expression;
    }
    public Integer getResult() { return result; }

    private ArithmeticExpression(ArithmeticOperationType operation, Integer a, Integer b) {
        this.result = operation.calculate(a, b);
        expression = a + operation.getName() + b + "=" + result;
    }
}
