package com.twuc.webApp.model;

public class ArithmeticCheckResult {

    public static ArithmeticCheckResult make(Boolean value) {
        ArithmeticCheckResult result = new ArithmeticCheckResult();
        result.setCorrect(value);
        return result;
    }

    private Boolean correct;

    private void setCorrect(Boolean correct) {
        this.correct = correct;
    }

    public Boolean getCorrect() {
        return correct;
    }
}
