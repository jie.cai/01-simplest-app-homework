package com.twuc.webApp.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.twuc.webApp.ArithmeticCheckType;
import com.twuc.webApp.ArithmeticOperationType;

import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArithmeticCheckRequest {
    @NotNull
    private Integer operandLeft;
    @NotNull
    private Integer operandRight;
    @NotNull
    private Integer expectedResult;
    @NotNull
    private ArithmeticOperationType operation;

    public ArithmeticOperationType getOperation() {
        return operation;
    }

    private ArithmeticCheckType checkType = ArithmeticCheckType.EQUAL;

    public Integer getOperandLeft() {
        return operandLeft;
    }

    public Integer getOperandRight() {
        return operandRight;
    }

    public ArithmeticCheckType getCheckType() {
        return checkType;
    }

    public Integer getExpectedResult() {
        return expectedResult;
    }
}
