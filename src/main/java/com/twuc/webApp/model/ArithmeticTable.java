package com.twuc.webApp.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.function.BiFunction;
import java.util.stream.IntStream;

public class ArithmeticTable {
    public static String plain(Integer start, Integer end, BiFunction<Integer, Integer, ArithmeticExpression> fn) {
        StringBuilder temp = new StringBuilder();
        for (int a = start; a <= end; a++) {
            for (int b = start; b <= a; b++) {
                if (b != start) {
                    temp.append("\t");
                }
                temp.append(fn.apply(a, b).getExpression());
            }
            temp.append("\r\n");
        }
        return temp.toString();
    }

    public static String json(Integer start, Integer end, BiFunction<Integer, Integer, ArithmeticExpression> fn) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(IntStream
                .range(start, end+1)
                .mapToObj(i -> IntStream.range(start, i+1)
                        .mapToObj(j -> fn.apply(i, j).getExpression())
                        .toArray())
                .toArray(Object[][]::new));
    }

    public static String html(Integer start, Integer end, BiFunction<Integer, Integer, ArithmeticExpression> fn) {
        StringBuilder temp = new StringBuilder();
        temp.append("<ol>");
        for (int a = start; a <= end; a++) {
            temp.append("<li>" +
                    "<ol>");
            for (int b = start; b <= a; b++) {
                temp.append("<li>").append(fn.apply(a, b).getExpression()).append("</li>");
            }
            temp.append("</ol>" +
                    "</li>");
        }
        temp.append("</ol>");
        return temp.toString();
    }
}
